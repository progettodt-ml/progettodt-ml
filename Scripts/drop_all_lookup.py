import pandas as pd

temp = pd.read_csv('lookup_state2.csv')
temp.drop('ColorName1', axis = 1, inplace = True)
temp.drop('ColorName2', axis = 1, inplace = True)
temp.drop('ColorName3', axis = 1, inplace = True)
temp.drop('sName', axis = 1, inplace = True)
temp.to_csv('train6.csv', index = False)
