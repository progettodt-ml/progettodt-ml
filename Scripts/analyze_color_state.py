import pandas as pd
import numpy as np

color = pd.read_csv('color_labels.csv')
animals = pd.read_csv('train4.csv')

print(animals.count())
print(color.count())

print(animals.groupby('Color1').Color1.count())
print(animals.groupby('Color2').Color1.count())
print(animals.groupby('Color3').Color1.count())

state = pd.read_csv('state_labels.csv')
print(state.count())
print(animals.groupby('State').State.count())
common = pd.merge(animals, state, left_on='State', right_on='StateID')
print(len(animals[~animals.State.isin(common.State)]))
print(state[~state.StateID.isin(common.State)])

