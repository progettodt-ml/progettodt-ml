import csv
import pandas as pd

def numberize():
	with open('train4.csv', 'r') as infile, open('numbers.csv', 'w') as outfile:
		reader = csv.reader(infile)
		header = next(reader)
		writer = csv.DictWriter(outfile,fieldnames=header,lineterminator = '\n')
		print(header)
		writer.writeheader()
		for row in reader:
			if(row[1] == 'NoName'):
				#scrivo zero
				i = 0
				new_row = {}
				for column in header:
					if(column=="Name"):
						new_row[column] = '0'
					else:
						new_row[column] = row[i]
					i = i+1
				writer.writerow(new_row)
				print(new_row)
			else:
			#scrivo uno
				new_row = {}
				i = 0
				for column in header:
					if(column=="Name"):
						new_row[column] = '1'
					else:
						new_row[column] = row[i]
					i = i+1
				writer.writerow(new_row)

def dropID():
	temp = pd.read_csv('numbers.csv')
	temp.drop('PetID', axis = 1, inplace = True)
	temp.to_csv('numbers2.csv', index = False)

dropID()
