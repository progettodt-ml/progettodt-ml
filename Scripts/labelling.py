import csv

with open('AdoptionSpeed.csv', 'r') as infile, open('labels2.csv', 'w') as outfile:
	reader = csv.reader(infile)
	writer = csv.writer(outfile)
	for row in reader:
		if row[1] in ('0', '1', '2', '3'):
			writer.writerow([row[0], 1])
		elif (row[1]=="AdoptionSpeed"):
			writer.writerow(row)
		else:	
			writer.writerow([row[0], 2])

