import csv

with open('C:\\Users\\Stefano\\Desktop\\RepoGitlab\\progettodt-ml\\lookup_color2.csv', 'r', encoding="utf-8") as infile, open('C:\\Users\\Stefano\\Desktop\\RepoGitlab\\progettodt-ml\\lookup_color3.csv', 'w', encoding="utf-8") as outfile:
	reader = csv.reader(infile)
	header = next(reader)
	writer = csv.DictWriter(outfile,fieldnames=header,lineterminator = '\n')
	writer.writeheader()
	print(header)
	for row in reader:
		new_row = {}
		i = 0
		for column in header:
			if(column == "Color1"):
				stringa =  row[20]
				new_row[column] = stringa.rstrip()
			elif(column == "Color2"):
				stringa = row[21]
				new_row[column] = stringa.rstrip()
			elif(column == "Color3"):
				stringa = row[22]
				new_row[column] = stringa.rstrip()
			else:
				new_row[column] = row[i]
			i = i+1
		writer.writerow(new_row)
		print(new_row)
