import pandas as pd
import numpy as np

breed = pd.read_csv('breed_labels.csv')
animals = pd.read_csv('train.csv')

print(animals.count())
print(breed.count())
print(animals.groupby('Breed1').Breed1.count())
grouped = animals.groupby('Breed1')
print(len(grouped))

infos = np.concatenate((animals.Breed1, breed.BreedID))
print(len(infos))
infos = np.sort( np.unique(infos) ) 
print(len(infos))

common = pd.merge(animals, breed, left_on='Breed1', right_on='BreedID')
#breed di train non presenti in breed_labels
print(animals[~animals.Breed1.isin(common.Breed1)])
#breed di breed_labels non presenti in train
print(breed[~breed.BreedID.isin(common.Breed1)])

