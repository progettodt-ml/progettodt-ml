import csv
import pandas as pd

def printing(file):
	with open(file, 'r') as infile:
		count = 0
		reader = csv.reader(infile)
		for row in reader:
			if(count in [392, 1046, 3220, 7286, 7707]):
				print('Breed1: '+row[3]+' Breed2: '+row[4])
			count = count +1

def editing():
	animals = pd.read_csv('train3.csv')
	animals.at[391, 'Breed1'] = animals.at[391,'Breed2']
	animals.at[391, 'Breed2'] = '0'
	animals.at[1045, 'Breed1'] = animals.at[1045,'Breed2']
	animals.at[1045, 'Breed2'] = '0'
	animals.at[3219, 'Breed1'] = animals.at[3219,'Breed2']
	animals.at[3219, 'Breed2'] = '0'
	animals.at[7285, 'Breed1'] = animals.at[7285,'Breed2']
	animals.at[7285, 'Breed2'] = '0'
	animals.at[7706, 'Breed1'] = animals.at[7706,'Breed2']
	animals.at[7706, 'Breed2'] = '0'
	animals.to_csv('train4.csv')

	
editing()
