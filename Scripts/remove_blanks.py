import csv

with open('C:\\Users\\Stefano\\Desktop\\RepoGitlab\\progettodt-ml\\lookup_state.csv','r', encoding="utf8") as infile, open('C:\\Users\\Stefano\\Desktop\\RepoGitlab\\progettodt-ml\\lookup_state2.csv', 'w', encoding="utf8") as outfile:
	reader = csv.reader(infile)
	header = next(reader)
	writer = csv.DictWriter(outfile,fieldnames=header,lineterminator = '\n')
	writer.writeheader()
	for row in reader:
		new_row = {}
		i = 0
		for column in header:
			if(column == "Name"):
				stringa =  row[1]
				new_row[column] = stringa.rstrip()
			elif(column == "Breed1"):
				stringa = row[3]
				new_row[column] = stringa.rstrip()
			elif(column == "Breed2"):
				stringa = row[4]
				new_row[column] = stringa.rstrip()
			elif(column == "Color1"):
				stringa = row[20]
				new_row[column] = stringa.rstrip()
			elif(column == "Color2"):
				stringa = row[21]
				new_row[column] = stringa.rstrip()
			elif(column == "Color3"):
				stringa = row[22]
				new_row[column] = stringa.rstrip()
			elif(column == "State"):
				stringa = row[23]
				new_row[column] = stringa.rstrip()
			else:
				new_row[column] = row[i]
			i = i+1
		writer.writerow(new_row)
		print(new_row)
