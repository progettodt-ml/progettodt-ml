import csv
import pandas as pd
import numpy as np

animals = pd.read_csv('train2.csv')
infos = np.sort(np.unique(animals.PetID))
print("Unique animals: {}".format(len(infos)))
print("Count degli elementi:")
print(animals.count())
print("\nPrime 5 righe del dataset:")
print(animals.head())

with open('train2.csv','r') as infile:
	reader = csv.reader(infile)
	seen = set()
	for row in reader:
		if(row[1] == "No Name Yet"):
			seen.add(row[21])
	count = 0
	for el in seen:
		count = count + 1;
	print("Hanno nome \"No Name Yet\": {} istanze".format(count))




