import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import scale

df = pd.read_csv('numbers4.csv')
#target = pd.read_csv('labels.csv')

#standardize the data
features = ['Type', 'Name', 'Age', 'Breed1', 'Breed2', 'Gender', 'Color1', 'Color2', 'Color3', 
'MaturitySize', 'FurLength', 'Vaccinated', 'Dewormed', 'Sterilized', 'Health', 'Quantity', 
'Fee', 'State']

x = df.loc[:, features].values
y = df.loc[:, ['Target']].values

#x = scale(x)
#X_std = StandardScaler().fit_transform(x)
x = StandardScaler().fit_transform(x)

print(pd.DataFrame(data = x, columns = features).head())

pca = PCA(n_components=10)
principalComponents = pca.fit_transform(x)

principalDf = pd.DataFrame(data = principalComponents, columns = ['pc1', 'pc2', 'pc3', 'pc4', 'pc5', 'pc6', 'pc7', 'pc8', 'pc9', 'pc10'])

finalDF = pd.concat([principalDf, df[['Target']]], axis = 1)
finalDF.to_csv('pca.csv',index = False)
print(finalDF.head())
